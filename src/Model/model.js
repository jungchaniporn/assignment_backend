const mongoose = require('mongoose');

// const Schema = mongoose.Schema;

const UserSchema = new mongoose.Schema({
  contactID: {
    type: Number,
    Required: 'Please enter',
  },
  firstName: {
    type: String,
    Required: 'Please enter',
  },
  lastName: {
    type: String,
    Required: 'Please enter',
  },
  gender: {
    type: String,
    default: 'Please enter',
  },
  email: {
    type: String,
    Required: 'Please enter',
  },
  mobile: {
    type: String,
    Required: 'Please enter',
  },
  facebook: {
    type: String,
    Required: 'Please enter',
  },
  imageURL: {
    type: String,
    Required: 'Please enter',
  },

});

module.exports = mongoose.model('User', UserSchema);
