const router = require('express').Router();
const Controller = require('../Controller/controller');

router.get('/searchByFirstName/:firstName', Controller.searchDataByfirstName);

router.get('/searchByID/:contactID', Controller.searchDataBycontactID);

router.get('/listAll', Controller.listAllData);

router.post('/mockDatabase', Controller.mockDatabase);

router.post('/add', Controller.addData);

router.put('/update/:contactID', Controller.updateData);

router.delete('/delete/:contactID', Controller.deleteData);

module.exports = router;
